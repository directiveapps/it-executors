<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>IT Executors</title>
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/agency.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body id="page-top">

  <!-- Navigation -->
   <div style="height: 150px; overflow: hidden;">
  <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
    <path d="M0.00,92.27 C216.83,192.92 304.30,8.39 500.00,109.03 L500.00,0.00 L0.00,0.00 Z" style="stroke: none;fill: #1a1a1a;"></path>
  </svg>
</div>
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Logo</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#portfolio">Sevices</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#work">Work</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about1">About us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact1">Contact us</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact1">Testimonails</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
  <div class = "overlay-1">
    <div class="container">
      <div class="intro-text">
		<div class = "row">
			<div class = "col-md-6"></div>
			<div class = "col-md-6">
				<h5>IT Executors</h5>
				<hr class="hrcolor hrheader">
				<h1 class="intro-lead-in">We Execute Solutions</h1>
				<p class = "intro">Lorem Ipsum is simply dummy text of the printing and typesetting industry ever since the 1500s.<p>
				<hr class="hrcolor hrheader">
			</div>
		</div>
      </div>
    </div>
<!-- <div style="height: 150px; overflow: hidden;transform: rotate(180deg);"> -->
  <!-- <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;"> -->
    <!-- <path d="M0.00,92.27 C216.83,192.92 304.30,8.39 500.00,109.03 L500.00,0.00 L0.00,0.00 Z" style="stroke: none;fill: #1a1a1a;"></path> -->
  <!-- </svg> -->
<!-- </div> -->
<img src  = "img/wave.png" id = "secondImg">
</div>
  </header>
<div>
	<section class="page-section" id="about1">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 text-left">
          <h5>about</h5>
          <p class = "white">Lorem Ipsum is simply dummy text of the
printing and typesetting industry. Lorem Ipsum
has been the industry's </p>
<hr class  = "hrcolor" />
<p class = "white">Lorem Ipsum is simply dummy text of the
printing and typesetting industry. Lorem Ipsum
has been the industry's standard dummy text
ever since the 1500s, when an unknown printer
took a galley of type and scrambled it to make
a type specimen book.</p>
        </div>
		<div class = "col-md-7">
			<div class="row text-center">
				<div class="col-md-12">
 
					 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active">
          <div class="carousel-caption">
          <div class = "row">
				<div class = "col-md-6 col-6 no_adding">
					<div class = "slider-1">
						<img src="img/team/1.jpg" alt="Los Angeles" class = "rounded-circle sliderImg">
						<h4 class = "sliderHead">Rose John</h4>
						<p class = "sliderSubHead">Developer</p><hr class="hrcolor hrcenter hrsmall">
						<p class="sliderText">Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry</p>
					</div>
					
				</div>
				<div class = "col-md-6 col-6 no_adding">
					<div class = "slider-1">
						<img src="img/team/2.jpg" alt="Los Angeles" class = "rounded-circle sliderImg">
						<h4 class = "sliderHead">Rose John</h4>
						<p class = "sliderSubHead">Developer</p><hr class="hrcolor hrcenter hrsmall">
						<p class="sliderText">Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry</p>
					</div>
					
				</div>
			</div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="carousel-caption">
          <div class = "row">
				<div class = "col-md-6 col-6 no_adding">
					<div class = "slider-1">
						<img src="img/team/1.jpg" alt="Los Angeles" class = "rounded-circle sliderImg">
						<h4 class = "sliderHead">Rose John</h4>
						<p class = "sliderSubHead">Developer</p><hr class="hrcolor hrcenter hrsmall">
						<p class="sliderText">Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry</p>
					</div>
					
				</div>
				<div class = "col-md-6 col-6 no_adding">
					<div class = "slider-1">
						<img src="img/team/2.jpg" alt="Los Angeles" class = "rounded-circle sliderImg">
						<h4 class = "sliderHead">Rose John</h4>
						<p class = "sliderSubHead">Developer</p><hr class="hrcolor hrcenter hrsmall">
						<p class="sliderText">Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry</p>
					</div>
					
				</div>
			</div>
          </div>
        </div>
		<div class="carousel-item">
          <div class="carousel-caption">
          <div class = "row">
				<div class = "col-md-6 col-6 no_adding">
					<div class = "slider-1">
						<img src="img/team/1.jpg" alt="Los Angeles" class = "rounded-circle sliderImg">
						<h4 class = "sliderHead">Rose John</h4>
						<p class = "sliderSubHead">Developer</p><hr class="hrcolor hrcenter hrsmall">
						<p class="sliderText">Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry</p>
					</div>
					
				</div>
				<div class = "col-md-6 col-6 no_adding">
					<div class = "slider-1">
						<img src="img/team/2.jpg" alt="Los Angeles" class = "rounded-circle sliderImg">
						<h4 class = "sliderHead">Rose John</h4>
						<p class = "sliderSubHead">Developer</p><hr class="hrcolor hrcenter hrsmall">
						<p class="sliderText">Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum is simply dummy text of
the printing and typesetting industry</p>
					</div>
					
				</div>
			</div>
          </div>
        </div>
	</div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
				</div>
			  </div>
		</div>
      </div>
    </div>
</section>
  </div>

  <section class="page-section" id="services">
    <div class="container">
      <div class="row">
	    <div class="col-lg-12 text-right"><h5>services</h5></div>
		<div class="col-lg-6 text-center">
			<div class = "row">
				<div class = "col-md-3 col-4">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" viewBox="0 0 477.055 327.096">
					  <g id="Group_2617" data-name="Group 2617" transform="translate(-411.883 -1045)">
						<g id="Group_2579" data-name="Group 2579" transform="translate(353.762 977.31)">
						  <g id="noun_developer_2549191" transform="translate(173.026 148.102)">
							<path id="Path_10" data-name="Path 10" d="M74.114,126.014l11.293-13.552L40.6,75.123l44.8-37.339L74.114,24.232,21.176,68.347a8.816,8.816,0,0,0,0,13.552Z" transform="translate(-18 -8.736)" fill="#33d9bf"/>
							<path id="Path_11" data-name="Path 11" d="M49.653,126.014,102.591,81.9a8.816,8.816,0,0,0,0-13.552L49.653,24.232,38.36,37.784l44.8,37.339-44.8,37.339Z" transform="translate(141.276 -8.736)" fill="#33d9bf"/>
							<rect id="Rectangle_54" data-name="Rectangle 54" width="134.672" height="17.646" transform="matrix(0.524, -0.852, 0.852, 0.524, 80.716, 114.702)" fill="#33d9bf"/>
						  </g>
						</g>
						<g id="noun_Laptop_484168" transform="translate(408 1028.7)">
						  <path id="Path_4387" data-name="Path 4387" d="M403.6,277.394a8.7,8.7,0,0,1-8.736,8.735H290.042c-2.912,8.25-10.677,14.559-19.9,14.559H208.511a20.825,20.825,0,0,1-19.9-14.559H83.787a8.736,8.736,0,1,1,0-17.471H196.378a8.7,8.7,0,0,1,8.736,8.736v1.941A3.828,3.828,0,0,0,209,283.218H270.63a3.828,3.828,0,0,0,3.882-3.882v-1.941a8.7,8.7,0,0,1,8.736-8.736H395.839C400.207,268.659,403.6,272.541,403.6,277.394Zm74.252,0V300.2A43.288,43.288,0,0,1,434.663,343.4H43.992A43.288,43.288,0,0,1,.8,300.2V277.394a8.7,8.7,0,0,1,8.736-8.736H36.227V44.448A28.168,28.168,0,0,1,64.375,16.3H413.8c15.044,0,27.662,11.647,29.118,27.177V268.659h26.692C474.458,268.659,477.855,272.541,477.855,277.394Zm-16.986,8.25H434.178a8.7,8.7,0,0,1-8.736-8.735V44.933c-.971-6.794-6.309-11.647-12.133-11.647H63.89A10.982,10.982,0,0,0,52.728,44.448V276.909a8.7,8.7,0,0,1-8.736,8.735H18.271V300.2A26.1,26.1,0,0,0,44.478,326.41H435.149A26.1,26.1,0,0,0,461.355,300.2V285.645Z" transform="translate(3.082 0)" fill="#33d9bf"/>
						</g>
					  </g>
					</svg>

				</div>
				<div class = "col-md-9 col-8 service">
					<h4>Lorem Ipsum</h4>
					<p class = "serviceText">simply dummy text of the printing and typesetting
industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s.</p>
				</div>
			</div>
			<div class = "row">
				<div class = "col-md-9 col-8 service">
					<h4>Lorem Ipsum</h4>
					<p class = "serviceText">simply dummy text of the printing and typesetting
industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s.</p>
				</div>
				<div class = "col-md-3 col-4">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" viewBox="0 0 477.055 327.096">
					  <g id="Group_2617" data-name="Group 2617" transform="translate(-411.883 -1045)">
						<g id="Group_2579" data-name="Group 2579" transform="translate(353.762 977.31)">
						  <g id="noun_developer_2549191" transform="translate(173.026 148.102)">
							<path id="Path_10" data-name="Path 10" d="M74.114,126.014l11.293-13.552L40.6,75.123l44.8-37.339L74.114,24.232,21.176,68.347a8.816,8.816,0,0,0,0,13.552Z" transform="translate(-18 -8.736)" fill="#33d9bf"/>
							<path id="Path_11" data-name="Path 11" d="M49.653,126.014,102.591,81.9a8.816,8.816,0,0,0,0-13.552L49.653,24.232,38.36,37.784l44.8,37.339-44.8,37.339Z" transform="translate(141.276 -8.736)" fill="#33d9bf"/>
							<rect id="Rectangle_54" data-name="Rectangle 54" width="134.672" height="17.646" transform="matrix(0.524, -0.852, 0.852, 0.524, 80.716, 114.702)" fill="#33d9bf"/>
						  </g>
						</g>
						<g id="noun_Laptop_484168" transform="translate(408 1028.7)">
						  <path id="Path_4387" data-name="Path 4387" d="M403.6,277.394a8.7,8.7,0,0,1-8.736,8.735H290.042c-2.912,8.25-10.677,14.559-19.9,14.559H208.511a20.825,20.825,0,0,1-19.9-14.559H83.787a8.736,8.736,0,1,1,0-17.471H196.378a8.7,8.7,0,0,1,8.736,8.736v1.941A3.828,3.828,0,0,0,209,283.218H270.63a3.828,3.828,0,0,0,3.882-3.882v-1.941a8.7,8.7,0,0,1,8.736-8.736H395.839C400.207,268.659,403.6,272.541,403.6,277.394Zm74.252,0V300.2A43.288,43.288,0,0,1,434.663,343.4H43.992A43.288,43.288,0,0,1,.8,300.2V277.394a8.7,8.7,0,0,1,8.736-8.736H36.227V44.448A28.168,28.168,0,0,1,64.375,16.3H413.8c15.044,0,27.662,11.647,29.118,27.177V268.659h26.692C474.458,268.659,477.855,272.541,477.855,277.394Zm-16.986,8.25H434.178a8.7,8.7,0,0,1-8.736-8.735V44.933c-.971-6.794-6.309-11.647-12.133-11.647H63.89A10.982,10.982,0,0,0,52.728,44.448V276.909a8.7,8.7,0,0,1-8.736,8.735H18.271V300.2A26.1,26.1,0,0,0,44.478,326.41H435.149A26.1,26.1,0,0,0,461.355,300.2V285.645Z" transform="translate(3.082 0)" fill="#33d9bf"/>
						</g>
					  </g>
					</svg>

				</div>
			</div>
        </div>
		<div class="col-lg-2"></div>
		<div class="col-lg-4 text-right">
			
          <h2 class="section-heading text-uppercase">Services we provide</h2>
		  <hr class="hrcolor hrheader"><br/>
          <p class = "white text-right pull-right">Lorem Ipsum is simply dummy text of the
printing and typesetting industry. Lorem
Ipsum has been the industry's standard
dummy text ever since the 1500s, when an
unknown printer took a galley</p>
        </div>
	</div>
	<section class="page-section" id="work">
   
	<div class = "row">
		<div class = "col-md-12 text-center">
		<br/><br/><br/>
			<h5>Work</h5>
			<hr class="hrcolor hrcenter hrsmall">
			<p class = "white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard
dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard
dummy text.
</p><br/><br/>
		</div>
	</div>
</section>
</div>
<div class = "col-md-12">			
	<div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators1" data-slide-to="0" class=""></li>
			<li data-target="#carouselExampleIndicators1" data-slide-to="1" class=""></li>
			<li data-target="#carouselExampleIndicators1" data-slide-to="2" class="active"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="carousel-item carousel-item1 active">
				<div class="carousel-caption carousel-caption1">
					<div class = "row">
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMNXW40dMQOYem6Z_CJIUrJ4jwgz5l32ZbzeyRaiu-qz_PtsOm" width = "100%"></div>
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShqQkz_K84DwyiCohoGBMmpujlivZViozvCkqdgvyQPGSHmEru" width = "100%"></div>
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSRIHUO9tUZruMw9dxTkGvqyMM6Jhq8GiHXBGwwi4ZP3VP1JR2" width = "100%"></div>
					</div>
				</div>
			</div>
			<div class="carousel-item carousel-item1">
				<div class="carousel-caption carousel-caption1">
					<div class = "row">
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMNXW40dMQOYem6Z_CJIUrJ4jwgz5l32ZbzeyRaiu-qz_PtsOm" width = "100%"></div>
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShqQkz_K84DwyiCohoGBMmpujlivZViozvCkqdgvyQPGSHmEru" width = "100%"></div>
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSRIHUO9tUZruMw9dxTkGvqyMM6Jhq8GiHXBGwwi4ZP3VP1JR2" width = "100%"></div>
					</div>
				</div>
			</div>
			<div class="carousel-item carousel-item1">
				<div class="carousel-caption carousel-caption1">
					<div class = "row">
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMNXW40dMQOYem6Z_CJIUrJ4jwgz5l32ZbzeyRaiu-qz_PtsOm" width = "100%"></div>
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShqQkz_K84DwyiCohoGBMmpujlivZViozvCkqdgvyQPGSHmEru" width = "100%"></div>
						<div class = "col-md-4 no-padding"><img src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSRIHUO9tUZruMw9dxTkGvqyMM6Jhq8GiHXBGwwi4ZP3VP1JR2" width = "100%"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
</section>

  <!-- Portfolio Grid -->
  <section class="" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
			<h5>feedback</h5>
          <h2 class="section-heading text-uppercase">What our customers are saying</h2>
		  <hr class = "hrcolor hrcenter" />
          <div class = "row">
			<div class = "col-md-4">
				<img class="rounded-circle img-fluid feedback_img" src="img/team/1.jpg" alt=""><br/><br/>
				<p class = "white">“Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum has been the industry's
standard dummy text ever since the
1500s, when an unknown.”</p>
<hr class = "hrcolor center">
<span class = "feedback_name white">John Doe- Happy Customer</span>			
</div>
<div class = "col-md-4">
				<img class="rounded-circle img-fluid feedback_img" src="img/team/2.jpg" alt=""><br/><br/>
				<p class = "white">“Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum has been the industry's
standard dummy text ever since the
1500s, when an unknown.”</p>
<hr class = "hrcolor center">
<span class = "feedback_name white">John Doe- Happy Customer</span>			
</div>
<div class = "col-md-4">
				<img class="rounded-circle img-fluid feedback_img" src="img/team/1.jpg" alt=""><br/><br/>
				<p class = "white">“Lorem Ipsum is simply dummy text of
the printing and typesetting industry.
Lorem Ipsum has been the industry's
standard dummy text ever since the
1500s, when an unknown.”</p>
<hr class = "hrcolor center">
<span class = "feedback_name white">John Doe- Happy Customer</span>			
</div>
		  </div>
        </div>
      </div>
    </div>
  </section>

  <section class="page-section" id="contact1">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
			<h5>contact</h5>
			<h2 class="section-heading text-uppercase">Connect with us</h2>
			<hr class="hrcolor center"><br/>
        </div>
		<div class="col-lg-5">
			<div class = "row">
				<div class = "col-md-1 col-2">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="57.81" viewBox="0 0 39.454 57.81">
					  <g id="noun_Location_2642737" transform="translate(0 0)">
						<g id="Group_2584" data-name="Group 2584">
						  <path id="Path_4372" data-name="Path 4372" d="M18.584,57.124C13.862,48.441,0,27.42,0,19.651a19.727,19.727,0,0,1,39.454,0c0,8.226-13.862,28.486-18.584,37.473A1.3,1.3,0,0,1,18.584,57.124Zm1.066-3.2c4.418-7.769,17.213-27.572,17.213-34.275a17.137,17.137,0,0,0-34.275,0C2.59,26.2,15.385,46.461,19.651,53.925Z" fill="#33d9bf"/>
						  <path id="Path_4373" data-name="Path 4373" d="M84.074,76A8.15,8.15,0,1,1,76,84.074,8.16,8.16,0,0,1,84.074,76Zm0,2.59a5.561,5.561,0,0,0,0,11.12,5.561,5.561,0,1,0,0-11.12Z" transform="translate(-64.423 -64.423)" fill="#33d9bf"/>
						</g>
					  </g>
					</svg>

				</div>
				<div class = "col-md-11 col-10"><p class = "white"> Itexecutors Consultancy Service Pvt Ltd 201, Nisarg Tulip, Renuka Akruti, Above Vita Life Clinic, Wakad Thergaon Road, Wakad Pune, MH, India - 411057.</p></div>
			</div>
			<div class = "row">
				<div class = "col-md-1 col-2">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="40.384" viewBox="0 0 40.345 40.384">
  <g id="noun_clock_2638304" transform="translate(0.506 0.535)">
    <g id="Group_2590" data-name="Group 2590" transform="translate(0 0)">
      <g id="Group_2587" data-name="Group 2587">
        <g id="Group_2586" data-name="Group 2586">
          <path id="Path_4375" data-name="Path 4375" d="M112.99,95.178c0,.5-.018,1.009-.059,1.514-.018.23-.041.455-.068.685-.009.063-.014.122-.023.185-.027.216.032-.212,0,0-.018.131-.041.266-.059.4a18.334,18.334,0,0,1-.685,2.816c-.144.437-.3.874-.478,1.3-.027.068-.045.225,0,0a.777.777,0,0,1-.072.167c-.041.09-.081.18-.122.275-.108.234-.221.469-.338.7a18.863,18.863,0,0,1-1.446,2.384,2.964,2.964,0,0,1-.388.523c.167-.144.041-.054,0,0s-.072.095-.108.14c-.072.09-.149.185-.225.275q-.453.547-.946,1.054c-.315.324-.64.64-.978.942-.167.149-.342.3-.514.442-.086.068-.167.14-.252.207-.045.036-.095.072-.14.108s-.063.05-.095.072l.041-.032a19.35,19.35,0,0,1-2.334,1.519c-.4.221-.811.424-1.226.617-.108.05-.216.1-.329.144-.054.023-.216.122,0,0-.05.027-.113.045-.167.068-.221.09-.446.176-.671.261a18.841,18.841,0,0,1-2.749.766c-.221.045-.442.086-.667.117l-.365.054c-.216.032.212-.027,0,0-.063.009-.122.014-.185.023-.5.059-1,.095-1.5.113a19.043,19.043,0,0,1-2.956-.122.862.862,0,0,1-.185-.023c.216.077.072.009,0,0-.1-.014-.2-.032-.306-.045-.252-.041-.5-.086-.757-.14q-.7-.142-1.392-.338c-.464-.131-.924-.284-1.379-.451-.207-.077-.41-.158-.617-.239-.072-.032-.216-.041,0,0a.385.385,0,0,1-.108-.05c-.126-.054-.257-.113-.383-.171a18.717,18.717,0,0,1-2.442-1.356c-.18-.117-.36-.243-.536-.369l-.288-.207c-.05-.036-.095-.072-.144-.108s-.275-.167-.054-.041a4.419,4.419,0,0,1-.5-.406c-.18-.153-.36-.306-.536-.464a18.723,18.723,0,0,1-1.92-2c-.077-.09-.149-.185-.225-.275-.036-.045-.072-.095-.108-.14-.126-.158.09.126.014.018-.144-.2-.3-.4-.437-.595-.261-.374-.509-.752-.743-1.145-.243-.41-.473-.825-.689-1.253q-.149-.291-.284-.595c-.05-.108-.1-.216-.144-.329-.009-.018-.09-.2-.027-.059s-.036-.09-.045-.113a19.083,19.083,0,0,1-.87-2.7q-.169-.716-.284-1.446c-.018-.113-.032-.225-.05-.338-.018-.135.027.234,0,.023-.009-.063-.014-.122-.023-.185-.027-.248-.054-.5-.072-.743a19.155,19.155,0,0,1,0-2.965c.018-.248.045-.5.077-.743a.829.829,0,0,1,.023-.185c0,.018-.032.221-.009.054.023-.144.041-.284.063-.428.077-.473.176-.946.288-1.41a19.276,19.276,0,0,1,.892-2.726c.027-.068.014-.122-.023.05a.777.777,0,0,1,.072-.167c.05-.108.1-.221.149-.329.1-.216.2-.433.311-.644.216-.424.446-.838.694-1.248.234-.388.487-.766.752-1.14a2.807,2.807,0,0,1,.388-.523c-.167.14-.045.054,0,0s.072-.095.108-.14c.086-.108.176-.216.261-.32a18.719,18.719,0,0,1,1.933-1.987c.162-.144.324-.284.491-.424.09-.077.185-.149.275-.225.032-.023.063-.05.095-.072s.063-.05.095-.072c-.1.081-.117.09-.041.032.378-.284.766-.559,1.163-.816a19.031,19.031,0,0,1,2.451-1.343c.09-.041.18-.081.275-.122.054-.023.216-.122,0,0,.05-.027.113-.045.167-.068.221-.09.446-.176.671-.261.455-.167.915-.311,1.383-.442q.676-.189,1.365-.324c.243-.05.482-.09.725-.131.1-.018.2-.032.306-.045.216-.032-.212.027,0,0,.081-.009.167-.023.248-.032a19.116,19.116,0,0,1,2.96-.1c.478.023.96.059,1.437.117a.864.864,0,0,1,.185.023c-.216-.077-.072-.009,0,0l.365.054c.243.041.487.086.725.135a18.1,18.1,0,0,1,2.74.784c.207.077.41.158.617.239.068.027.225.045,0,0a.776.776,0,0,1,.167.072c.117.054.239.1.356.158.424.2.838.41,1.248.635s.82.478,1.217.739c.18.122.36.243.536.369.09.063.176.126.261.189.041.032.081.059.117.09s.275.171.054.041a4.323,4.323,0,0,1,.5.406c.2.162.392.333.581.5q.507.453.973.946c.329.342.64.694.942,1.059.063.077.126.153.185.23.036.045.072.095.108.14.131.162-.122-.162,0,0,.144.189.284.379.419.572a19.2,19.2,0,0,1,1.433,2.393c.108.212.212.428.306.649.041.09.081.18.122.275.023.054.122.216,0,0a1.891,1.891,0,0,1,.09.221c.171.428.329.861.473,1.3a18.589,18.589,0,0,1,.662,2.789,2.513,2.513,0,0,1,.054.369c0-.252-.009-.054,0,0s.014.122.023.185c.027.225.05.455.063.685.032.514.05,1,.05,1.482a.9.9,0,1,0,1.8,0,19.647,19.647,0,0,0-3.6-11.337,19.931,19.931,0,0,0-9.363-7.155,19.67,19.67,0,0,0-14.843,36.4,19.96,19.96,0,0,0,12.107,1.365,19.687,19.687,0,0,0,15.451-16.158,21.185,21.185,0,0,0,.252-3.114.91.91,0,0,0-.9-.9A.923.923,0,0,0,112.99,95.178Z" transform="translate(-75.458 -75.51)" fill="#33d9bf" stroke="#32d6bc" stroke-width="1"/>
        </g>
      </g>
      <g id="Group_2589" data-name="Group 2589" transform="translate(18.706 6.113)">
        <g id="Group_2588" data-name="Group 2588">
          <path id="Path_4376" data-name="Path 4376" d="M490.6,212.093v12.026c0,.446-.068.951.306,1.28.414.37.87.7,1.3,1.05l2.37,1.911q1.892,1.528,3.789,3.05l.068.054a.924.924,0,0,0,1.275,0,.911.911,0,0,0,0-1.275l-2.564-2.064q-2.041-1.642-4.078-3.285c-.311-.252-.622-.5-.933-.752.09.212.176.424.266.635V212.084a.91.91,0,0,0-.9-.9.93.93,0,0,0-.9.91Z" transform="translate(-490.594 -211.182)" fill="#33d9bf" stroke="#32d6bc" stroke-width="1"/>
        </g>
      </g>
    </g>
  </g>
</svg>

				</div>
				<div class = "col-md-11 col-10"><p class = "white">Mon-Fri 10.30 - 19.30 <br> Saturday & Sunday CLOSED</p></div>
			</div>
			<div class = "row">
				<div class = "col-md-1 col-2">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="40.52" viewBox="0 0 40.564 40.52">
					  <g id="noun_Phone_153863" transform="translate(0.519 0.5)">
						<path id="Path_4377" data-name="Path 4377" d="M40.646,49.72h0a9.192,9.192,0,0,0,4.965-1.44c2.681-1.688,4.071-3.624,4.121-5.66.149-3.277-2.929-6.107-6.256-8.688a4.229,4.229,0,0,0-2.482-.794,8.04,8.04,0,0,0-6.107,3.426c-.645-.05-2.234-1.589-2.78-2.185l-6.554-6.554c-1.44-1.44-2.185-2.383-2.185-2.78a8.328,8.328,0,0,0,3.376-5.312A4.051,4.051,0,0,0,26,16.456c-2.482-3.277-5.263-6.256-8.44-6.256-2.135,0-4.121,1.39-5.908,4.17C8.275,19.633,10.757,26.485,19,34.726l6.256,6.256C27.39,43.117,33.993,49.72,40.646,49.72Zm.348-14.547a2.1,2.1,0,0,1,1.291.4c2.78,2.135,5.61,4.617,5.511,7-.05,1.39-1.142,2.78-3.227,4.071A7.2,7.2,0,0,1,40.7,47.784c-5.809,0-11.965-6.156-14-8.192l-6.256-6.256c-7.5-7.5-9.88-13.5-7.05-17.873,1.39-2.135,2.78-3.227,4.22-3.227,2.383,0,4.816,2.78,6.9,5.511a2.648,2.648,0,0,1,.348,1.787,6.468,6.468,0,0,1-2.582,4.022,1.847,1.847,0,0,0-.794,1.39c-.1,1.39,1.241,2.88,2.78,4.419l6.4,6.454c1.44,1.489,2.88,2.78,4.22,2.78a1.839,1.839,0,0,0,1.539-.794A6.239,6.239,0,0,1,40.993,35.173Z" transform="translate(-10.19 -10.2)" fill="#33d9bf" stroke="#33d9bf" stroke-width="1"/>
					  </g>
					</svg>

				</div>
				<div class = "col-md-11 col-10"><p class = "white">+91 7875433533</p></div>
			</div>
			<div class = "row">
				<div class = "col-md-1 col-2">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30.899" viewBox="0 0 40.544 30.899">
					  <g id="noun_Email_2646585" transform="translate(0.5 0.5)">
						<g id="Group_2583" data-name="Group 2583">
						  <path id="Path_4371" data-name="Path 4371" d="M3.751,0H35.793a3.8,3.8,0,0,1,3.751,3.751v22.5A3.705,3.705,0,0,1,35.793,29.9H3.751A3.705,3.705,0,0,1,0,26.256V3.751A3.8,3.8,0,0,1,3.751,0ZM36.222,28.077,22.4,15.86l-1.715,1.5a.841.841,0,0,1-1.179,0l-2.143-1.715L3.322,28.077h32.9ZM1.929,26.9,15.968,14.467,1.929,3a1.589,1.589,0,0,0-.107.75v22.5A1.359,1.359,0,0,0,1.929,26.9Zm1.5-25.077L20.147,15.539,36.115,1.822H3.429ZM37.615,3,23.791,14.682,37.615,26.9c0-.214.107-.429.107-.643V3.751C37.722,3.429,37.615,3.215,37.615,3Z" fill="#33d9bf" stroke="#33d9bf" stroke-width="1"/>
						</g>
					  </g>
					</svg>

				</div>
				<div class = "col-md-11 col-10"><p class = "white">contactus@itexecutors.com</p></div>
			</div>
		</div>
		<div class="col-lg-1"></div>
        <div class="col-lg-6">
        	<div id="success"></div>
          <form id="contactForm" action="" name="sentMessage" novalidate="novalidate" >
			<div class="form-group">
			  <input class="form-control" id="name" name="name" type="text" placeholder="Full Name" required="required" data-validation-required-message="Please enter your name.">
			  <p class="help-block text-danger"></p>
			</div>
			<div class="form-group">
			  <input class="form-control" id="email" name="email" type="email" placeholder="Email" required="required" data-validation-required-message="Please enter your email address.">
			  <p class="help-block text-danger"></p>
			</div>
			<div class="form-group">
			  <input class="form-control" id="phone" name="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number.">
			  <p class="help-block text-danger"></p>
			</div>
		  
			<div class="form-group">
			  <textarea class="form-control" id="message" name="message" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
			  <p class="help-block text-danger"></p>
			</div>
			<div class="form-group text-center">
				<h5><input type="submit" id="sendMessageButton" name="submit" class = "button" value="SEND"></h5>
			</div>
          </form>
        </div>
      </div>
    </div>
  </section>

<footer class="footer">
	<!-- <div id="wave"></div> -->
  <div class = "footer-color" >
  
<!-- <div style="height: 150px; overflow: hidden;"> -->
<!-- <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;transform: scaleX(-1);"> -->
    <!-- <path d="M0.00,-1.27 C216.83,222.92 200.30,-191.39 500.00,149.03 L700.00,0.00 L0.00,0.00 Z" style="stroke: none;fill: #1a1a1a;"></path> -->
  <!-- </svg> -->
<!-- </div> -->
<img src  = "./img/waves1.png" style = "width:100%">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-12">
			
          
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50" viewBox="0 0 58 58">
  <image id="icons8-twitter-squared-96" width="58" height="58" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAHugAAB7oBj/ZIjgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAlQSURBVHic7ZxrcFxlGcd/z9kNbXI2DbUDUhwuWku1ZQrYbELaInWQDna4ConJJi0XL8hlhBkGEMdLdZRxOqCOVtAPTC1tNjWtjgyC5TK2MGLb3a0IwpRwGQoCMkULzZ6TZpPd8/ghiZOJbbJnzzm7m3J+nzbnvO//eXKes+/7vJd9ISQkJCQkJCQkJCQkJCQkJCQkJCQkJCTkWEcq7UDQJLPxRQIXqOi5iiwQOBWYAyhwENgP9KnqblF9MjFr70t+2O3pjy9VQy5PxFJ3TFbumAxAr7ZGCtb+qxS+gbDUZfUMyPqsWUheL3uH3dpODjS1SIE7VfQyVWnrrE9tnaz8MReApB2/BOVeYL4nIeE1VG9IxDJPTFV0g66YOcO2O4CbgCUAIjzTXpc+TwSd3MwxwoO62IzYM+8XdLWfuiLcH6mL3tYmuw6Pv96rrZGCvX+FQifwRaBhXCWrUHAaV8/K9E2p79ah3oGWj+Wd/E2JWPpbbusGxSZ7ydyIGo8A5wRkIgPGxZqvGTIigxcoshLhYmDuEcqqqiY66zNbihGOuvWkoPlVwF09dtObHWbqV27r+02vFT8p7/AXhE8EaKYRnD6J5mKKRCYrKLAuUeTDBzBcu+LQCKCq6zfbTRe7ru8jvdpSm4eHA374YzQAkz58hPv6THctg+sAKJw5+jFiqG7pyTaucKvhFwU7fx+MvBBVwI8TZvqmtYLjppL7b4Bw6ri/TEfkkS1W80rXOh7pthtXKVxTbrtHoAB6RyKWvmvsQu+BRbGk3Xh9txW/ZarKrjrhtYpxhh0fBGom3Bp0RFq7zNQf3eiVSq+2RvL2/heBBeWwNwnviGpnR31mZ/eh5vkScZaLymUquhJ4M2/mlqyR5+3JBFwF4EFdbEbtGdZRbg8pXN8ZS//GjWYpJK14F7ApaDtTMKywVeAE4CzgxP/dEbEMZXl7LPXcVCKuAtB7qOUj+Uj+P1MU2xw1o1+bmDf7SdKKZxgd8FQhOVG9qKM+s7OYwq76gOyQUczQvCtv5x/rteInudEulu5DzfOp3ofvADcU+/DBZQC+fMIzWWDSNm2U8/LwXLcV73SjXwyGUSh7h18keeDqRCy9wU0l91kQvFxkuRMFNndbjY91D8Z9y9NVWOaXlo/YiLQmYunNbiuWEoDtbgoLslLyvNCTjf9gY3/TnBLsTVT8tHcN/xCRVx2cloSZ+kMp9V0HwBBnWwl2alX4Tk2E/UmraV3Sav5oCRpjnOKhrq8oDAwODcW7Ynv/UapGSbOhSSv+OHBhqUaBwyA9ivbUmKfvaJOtBRe2h/j/cUilOJyIpeu8CJQUgJ6BJc3qGM8w1dxIcRxA2KaOsW0oVrvrWtk5OFnhpBWfdH69zOQTsbSnl6Hk9YCk1bQO9HYvxo9AToQMyLOK84KqvCYYB0SG/u0Mm4cBJJo76LNNLziJWNrTS+h6OnqMnFn33ZkD9lJVX7OSGSN6ugxk9O1wUI0i0ZyPZnzD9ZLlRErJggC4VnYOFgaNVcDfvToxjcl6FSg5AABdc/b0R+ELwF6vjkxTjjYvVjSeAgDQFku/mzdz54vKQ161piHl/wZseP/s4ydeWyPP2+2x1BWq3Kgw4NWpacS7XgVcB2BGTU2qx4pfMPG6CNpZn76fgnE2yKNeHZsWCK94lSihCZLjFJ5MZuN/TVrxa3vsc04ef7ezYc8r0QH7S6JyN7hbnptuqBY9L3ZU3Kehqn9DOA2hBWhRjZK04v9ER1MyoSEPs0E99y/VTkS0AgEQHgaumHD1lGNni1fxGCrPetZwWyE6MLAVmGpV7MNAX1ssXf5OuO3EFy3QH3o1PN0R5Sk/dEpqp182Mz8HnvbDgWmLSOUCsFZw8hG5hg9vUzQ85PCYH0IlZypralOvFxxdBrzthyPTjO1Xz0r58vJ5ShVXz8r0GRq5EOQNP5yZLihSyqrgEfGcq7fX796XGx46G+j1wZ/pwKDmpKT13yPha/bebcU7BfkR6Gl+6lYTgj7QEct8xS89X0ernbF09/HmwQUKtwLv+KldJTjq6D1+CgY2fu3V1khh4I2LQK9zlIsEPC1eVwOi8lBHfepyXzW9CmzQFTOnWkh/VD854wN79udAf1Jt+3pcYcjSRF1ql5+SngOQtOJPAAtAX1WMtwQ9oMh7hmiNqs4GmY3IfFQXA7O8u1whhN8lzPRVfsuWvCg/hiL3CLod5JSxX2QKiurIp5FC1bSTxD0KAyrGrUFoe+6EE2bqcdAdfjhTrRjwi666PW8FpO0NETRq1KzmmJ2WkDcKOePuoNR9SUPb6na9jXIlPixSVxkFVV3TNWdPf1AGfBsHJOrTTzmGsRJ43y/NKmBdZ3060FlfXwdiXXV7dovBWQp/8lO3QmSypvO9oI0ENhBLWo0dIN/H66EZleFtxzDODarjHU+gK7k7dEX0X/ZAJ+ga4Hz82U0dLCKW4Hy2w8x4Xu8tylw5jABs7G+aExXnQhHiII0Kp8vIwUlmuXwoggLCFQkz/XC5DFZ0L0OP3dSIaq/CxyvpxyjDglzTEUsly2m0IgFYqxgLrPjNKqwDZlTChwkcFpHWDjP1SLkNlz0Am7PNCw1Df43q8nLbPgqHVLk06HTzaJQtAJuzzQsNce4COqiazlj3GRq9sr1+975KeRBoAJL9Sz6lIp8XMbpAm4O05RqRbYcHaq4b/fF55dyYqkD3B8tnH+1eTcSO5hxpMKI1DagzG+RkA12osBBoArz8HDUocgLfbjfT9051oF45mHI62jCGzlTDWQ+yeOK9PFEiEUDHNkFr5f+jydmej8iNa2pTr3dU2pNRimqCenXRcXm77jbgm0zPRZWDInp7e11mQzW89eNx1Qds7G+aU2NwJ+jNQG1APvnJB6qsz6v8zK+NVH5TUie8yV4yN6rGLQpfZ/x5mdXDe8BPnZzxyyCnkv3AUxb0wHvL6mtrh78Keh2wyCefSsUB/gxsrDed318ie6fFb9V8S0O3WE1nOdCJ6pVlOkYSRg7gfh74rWMYm8oxe+k3gYwDtmQbzywYXCoqK4HPAPU+SSvwgsIOw5CdQ3merta2vVgCHwmvVYx52cb5hnCOGHIGqvNEZJ46zEVoYKQPiTJy4lQWOMTIqVy2wuuovGSg+xyhr8aM9gV5Fl1ISEhISEhISEhISEhISEhISEhISEhISEgQ/Bei7Rkef3065AAAAABJRU5ErkJggg=="/>
</svg>

              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="50" viewBox="0 0 30.522 58">
  <path id="icons8-facebook-f" d="M37.522,22.3H27.3V16.5c0-2.993.244-4.878,4.533-4.878H37.25V2.4A75.589,75.589,0,0,0,29.31,2C21.442,2,15.7,6.805,15.7,15.627V22.3H7V33.9l8.7,0V60H27.3V33.891l8.891,0Z" transform="translate(-7 -2)" fill="#a6f435"/>
</svg>

              </a>
            </li>
            <li class="list-inline-item">
            <li class="list-inline-item">
              <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50" viewBox="0 0 58 58">
  <image id="icons8-twitter-squared-96" width="58" height="58" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAHugAAB7oBj/ZIjgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAlQSURBVHic7ZxrcFxlGcd/z9kNbXI2DbUDUhwuWku1ZQrYbELaInWQDna4ConJJi0XL8hlhBkGEMdLdZRxOqCOVtAPTC1tNjWtjgyC5TK2MGLb3a0IwpRwGQoCMkULzZ6TZpPd8/ghiZOJbbJnzzm7m3J+nzbnvO//eXKes+/7vJd9ISQkJCQkJCQkJCQkJCQkJCQkJCQkJCTkWEcq7UDQJLPxRQIXqOi5iiwQOBWYAyhwENgP9KnqblF9MjFr70t+2O3pjy9VQy5PxFJ3TFbumAxAr7ZGCtb+qxS+gbDUZfUMyPqsWUheL3uH3dpODjS1SIE7VfQyVWnrrE9tnaz8MReApB2/BOVeYL4nIeE1VG9IxDJPTFV0g66YOcO2O4CbgCUAIjzTXpc+TwSd3MwxwoO62IzYM+8XdLWfuiLcH6mL3tYmuw6Pv96rrZGCvX+FQifwRaBhXCWrUHAaV8/K9E2p79ah3oGWj+Wd/E2JWPpbbusGxSZ7ydyIGo8A5wRkIgPGxZqvGTIigxcoshLhYmDuEcqqqiY66zNbihGOuvWkoPlVwF09dtObHWbqV27r+02vFT8p7/AXhE8EaKYRnD6J5mKKRCYrKLAuUeTDBzBcu+LQCKCq6zfbTRe7ru8jvdpSm4eHA374YzQAkz58hPv6THctg+sAKJw5+jFiqG7pyTaucKvhFwU7fx+MvBBVwI8TZvqmtYLjppL7b4Bw6ri/TEfkkS1W80rXOh7pthtXKVxTbrtHoAB6RyKWvmvsQu+BRbGk3Xh9txW/ZarKrjrhtYpxhh0fBGom3Bp0RFq7zNQf3eiVSq+2RvL2/heBBeWwNwnviGpnR31mZ/eh5vkScZaLymUquhJ4M2/mlqyR5+3JBFwF4EFdbEbtGdZRbg8pXN8ZS//GjWYpJK14F7ApaDtTMKywVeAE4CzgxP/dEbEMZXl7LPXcVCKuAtB7qOUj+Uj+P1MU2xw1o1+bmDf7SdKKZxgd8FQhOVG9qKM+s7OYwq76gOyQUczQvCtv5x/rteInudEulu5DzfOp3ofvADcU+/DBZQC+fMIzWWDSNm2U8/LwXLcV73SjXwyGUSh7h18keeDqRCy9wU0l91kQvFxkuRMFNndbjY91D8Z9y9NVWOaXlo/YiLQmYunNbiuWEoDtbgoLslLyvNCTjf9gY3/TnBLsTVT8tHcN/xCRVx2cloSZ+kMp9V0HwBBnWwl2alX4Tk2E/UmraV3Sav5oCRpjnOKhrq8oDAwODcW7Ynv/UapGSbOhSSv+OHBhqUaBwyA9ivbUmKfvaJOtBRe2h/j/cUilOJyIpeu8CJQUgJ6BJc3qGM8w1dxIcRxA2KaOsW0oVrvrWtk5OFnhpBWfdH69zOQTsbSnl6Hk9YCk1bQO9HYvxo9AToQMyLOK84KqvCYYB0SG/u0Mm4cBJJo76LNNLziJWNrTS+h6OnqMnFn33ZkD9lJVX7OSGSN6ugxk9O1wUI0i0ZyPZnzD9ZLlRErJggC4VnYOFgaNVcDfvToxjcl6FSg5AABdc/b0R+ELwF6vjkxTjjYvVjSeAgDQFku/mzdz54vKQ161piHl/wZseP/s4ydeWyPP2+2x1BWq3Kgw4NWpacS7XgVcB2BGTU2qx4pfMPG6CNpZn76fgnE2yKNeHZsWCK94lSihCZLjFJ5MZuN/TVrxa3vsc04ef7ezYc8r0QH7S6JyN7hbnptuqBY9L3ZU3Kehqn9DOA2hBWhRjZK04v9ER1MyoSEPs0E99y/VTkS0AgEQHgaumHD1lGNni1fxGCrPetZwWyE6MLAVmGpV7MNAX1ssXf5OuO3EFy3QH3o1PN0R5Sk/dEpqp182Mz8HnvbDgWmLSOUCsFZw8hG5hg9vUzQ85PCYH0IlZypralOvFxxdBrzthyPTjO1Xz0r58vJ5ShVXz8r0GRq5EOQNP5yZLihSyqrgEfGcq7fX796XGx46G+j1wZ/pwKDmpKT13yPha/bebcU7BfkR6Gl+6lYTgj7QEct8xS89X0ernbF09/HmwQUKtwLv+KldJTjq6D1+CgY2fu3V1khh4I2LQK9zlIsEPC1eVwOi8lBHfepyXzW9CmzQFTOnWkh/VD854wN79udAf1Jt+3pcYcjSRF1ql5+SngOQtOJPAAtAX1WMtwQ9oMh7hmiNqs4GmY3IfFQXA7O8u1whhN8lzPRVfsuWvCg/hiL3CLod5JSxX2QKiurIp5FC1bSTxD0KAyrGrUFoe+6EE2bqcdAdfjhTrRjwi666PW8FpO0NETRq1KzmmJ2WkDcKOePuoNR9SUPb6na9jXIlPixSVxkFVV3TNWdPf1AGfBsHJOrTTzmGsRJ43y/NKmBdZ3060FlfXwdiXXV7dovBWQp/8lO3QmSypvO9oI0ENhBLWo0dIN/H66EZleFtxzDODarjHU+gK7k7dEX0X/ZAJ+ga4Hz82U0dLCKW4Hy2w8x4Xu8tylw5jABs7G+aExXnQhHiII0Kp8vIwUlmuXwoggLCFQkz/XC5DFZ0L0OP3dSIaq/CxyvpxyjDglzTEUsly2m0IgFYqxgLrPjNKqwDZlTChwkcFpHWDjP1SLkNlz0Am7PNCw1Df43q8nLbPgqHVLk06HTzaJQtAJuzzQsNce4COqiazlj3GRq9sr1+975KeRBoAJL9Sz6lIp8XMbpAm4O05RqRbYcHaq4b/fF55dyYqkD3B8tnH+1eTcSO5hxpMKI1DagzG+RkA12osBBoArz8HDUocgLfbjfT9051oF45mHI62jCGzlTDWQ+yeOK9PFEiEUDHNkFr5f+jydmej8iNa2pTr3dU2pNRimqCenXRcXm77jbgm0zPRZWDInp7e11mQzW89eNx1Qds7G+aU2NwJ+jNQG1APvnJB6qsz6v8zK+NVH5TUie8yV4yN6rGLQpfZ/x5mdXDe8BPnZzxyyCnkv3AUxb0wHvL6mtrh78Keh2wyCefSsUB/gxsrDed318ie6fFb9V8S0O3WE1nOdCJ6pVlOkYSRg7gfh74rWMYm8oxe+k3gYwDtmQbzywYXCoqK4HPAPU+SSvwgsIOw5CdQ3merta2vVgCHwmvVYx52cb5hnCOGHIGqvNEZJ46zEVoYKQPiTJy4lQWOMTIqVy2wuuovGSg+xyhr8aM9gV5Fl1ISEhISEhISEhISEhISEhISEhISEhISEgQ/Bei7Rkef3065AAAAABJRU5ErkJggg=="/>
</svg>

              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="50" viewBox="0 0 30.522 58">
  <path id="icons8-facebook-f" d="M37.522,22.3H27.3V16.5c0-2.993.244-4.878,4.533-4.878H37.25V2.4A75.589,75.589,0,0,0,29.31,2C21.442,2,15.7,6.805,15.7,15.627V22.3H7V33.9l8.7,0V60H27.3V33.891l8.891,0Z" transform="translate(-7 -2)" fill="#a6f435"/>
</svg>

              </a>
            </li>
          </ul><br/>
		  <span class="copyright white">IT Executors Consultancy Services Pvt Ltd.</span>
        </div>
      </div>
    </div></div>
  </footer>
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/agency.min.js"></script>

</body>

</html>
